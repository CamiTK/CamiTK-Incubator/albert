/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2014 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK includes
#include "AlbertsRegionGrowing.h"
#include <Property.h>
#include <Application.h>
#include <ActionWidget.h>
#include <MultiPickingWidget.h>

// Qt includes
#include <QString>
#include <QMessageBox>
#include <QTextStream>

// Itk includes
#include <itkImage.h>
#include <itkConnectedThresholdImageFilter.h>
#include <itkImageToVTKImageFilter.h>

using namespace camitk;


// --------------- Constructor -------------------
AlbertsRegionGrowing::AlbertsRegionGrowing(ActionExtension * extension) : Action(extension) {
    // Setting name, description and input component
    setName("Albert's Region Growing");
    setDescription("This Action allows to apply a region growing algorithm on a 2D, unsigned char gray levels images.");
    setComponent("ImageComponent");

    // Setting classification family and tags
    setFamily("ExcellenceLab");
    // Tags allow the user to search the actions trhough themes
    // You can add tags here with the method addTag("tagName");

    // Setting the action's parameters
    addParameter(new Property(tr("Low Threshold"), 0, tr("Value below which the region does not grow any more"), "gray level"));
    addParameter(new Property(tr("High Threshold"), 255, tr("Value above which the region does not grow any more"), "gray level"));

	seedPoints = NULL;
}

// --------------- destructor -------------------
AlbertsRegionGrowing::~AlbertsRegionGrowing() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

QWidget * AlbertsRegionGrowing::getWidget() {
    // build or update the widget
    if ( !actionWidget ) {
        // Setting the widget containing the parameters, using the default widget
        actionWidget = new ActionWidget ( this );
        pickingW = new MultiPickingWidget(actionWidget);
        QLayout * widgetLayout = actionWidget->layout();
        QBoxLayout * boxWidgetLayout = dynamic_cast<QBoxLayout *>(widgetLayout);
        if (boxWidgetLayout != NULL) {
            boxWidgetLayout->insertWidget(2, pickingW);
        }
        else {
            actionWidget->layout()->addWidget(pickingW);
        }
    }
    // make sure the widget has updated targets
    dynamic_cast<ActionWidget*> ( actionWidget )->updateTargets();
    pickingW->updateComponent(dynamic_cast<ImageComponent *> (getTargets().last()));
    
    return actionWidget;
    
}

void AlbertsRegionGrowing::setSeedPoints(QList<QVector3D> * seedPoints) {
	this->seedPoints = seedPoints;
}

// --------------- apply -------------------
Action::ApplyStatus AlbertsRegionGrowing::apply() {

    ImageComponent * input = dynamic_cast<ImageComponent *> ( dynamic_cast<ImageComponent *> (getTargets().last()));
    
    return process(input);
}

Action::ApplyStatus AlbertsRegionGrowing::process(ImageComponent * comp) {
    // Get Parameters
    int lowThreshold  = property("Low Threshold").toInt();
    int highThreshold = property("High Threshold").toInt();
	if (seedPoints == NULL) {
		seedPoints = pickingW->getPickedPixelMap(comp)->getPixelIndexList();
	}
    
    // Getting the input image
    vtkSmartPointer<vtkImageData> inputImage  = comp->getImageData();
    
    // Check that the type is unsigned char and 2D image
    if (inputImage->GetScalarType() != VTK_UNSIGNED_CHAR) {
        QMessageBox::warning(NULL, "Warning", "This action is designed only for unsigned char type images\n sorry...\n");
        return ABORTED;
    }
    int * dims = inputImage->GetDimensions();
    if (dims[2] > 1) {
        QMessageBox::warning(NULL, "Warning", "This action is designed only for 2D images\n sorry...\n");
        return ABORTED;
    }
    if (seedPoints->empty()) {
        QMessageBox::warning(NULL, "Warning", "You did not pick any point. Your action may not work...\n");
        return ABORTED;
    }
    // Check parameters
//    QString msg = "parameters: low threshold " + QString::number(lowThreshold) + " high threshold " + QString::number(highThreshold);
//    QMessageBox::warning(NULL, "Method Region Growint", msg);
    
    
    typedef itk::Image< unsigned char, 2 >  ImageType;
    ImageType::Pointer image = ImageType::New();
    
    // Copy pixels values
    ImageType::RegionType region;
    ImageType::IndexType start;
    typename ImageType::IndexType pixelIndex;
    start[0] = 0;
    start[1] = 0;
    
    ImageType::SizeType size;
    size[0] = dims[0];
    size[1] = dims[1];
    
    region.SetSize( size);
    region.SetIndex(start);
    
    image->SetRegions(region);
    image->Allocate();
    
    // Make a square
    for(unsigned int j = 0; j < dims[1]; j++)
    {
        for(unsigned int i = 0; i < dims[0]; i++)
        {
            pixelIndex[0] = i;
            pixelIndex[1] = j;
            
            unsigned char val = (unsigned char) (inputImage->GetScalarComponentAsDouble(i, j, 0, 0));
            image->SetPixel(pixelIndex, val);
        }
    }
    
    typedef itk::ConnectedThresholdImageFilter<ImageType, ImageType> ConnectedFilterType;
    typename ConnectedFilterType::Pointer regionGrow = ConnectedFilterType::New();
    regionGrow->SetInput(image);
    regionGrow->SetLower(lowThreshold);
    regionGrow->SetUpper(highThreshold);
    
    regionGrow->SetReplaceValue(255);
    
    // set the initial seeds
    for (int i = 0; i < seedPoints->size(); i++) {
        pixelIndex[0] = seedPoints->at(i).x();
        pixelIndex[1] = seedPoints->at(i).y();
        regionGrow->AddSeed(pixelIndex);
    }
    regionGrow->Update();
    // Visualize
    typedef itk::ImageToVTKImageFilter<ImageType> ConnectorType;
    ConnectorType::Pointer backToVtk = ConnectorType::New();
    backToVtk->SetInput(regionGrow->GetOutput());
    backToVtk->Update();
    vtkSmartPointer<vtkImageData> outputImage = backToVtk->GetOutput();
    
    
    // Create The output Component
    QString newName;
    QTextStream(&newName) << comp->getName() << "_processed";
    QString newFileName;
    newFileName = QFileInfo(comp->getFileName()).path() + "/" + newName + ".mhd";

    ImageComponent * newComp = new ImageComponent(outputImage, newName);
    newComp->setFileName(newFileName);
    
    Application::refresh();
    
    return SUCCESS;
}


