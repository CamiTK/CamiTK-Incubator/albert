/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2014 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK includes
#include "PigSnoutSurface.h"
#include <Property.h>
#include <Application.h>

// Qt includes
#include <QString>
#include <QMessageBox>
#include <QTextStream>
#include <QVector3D>

#include "PigSnoutStatistics.h"


using namespace camitk;


// --------------- Constructor -------------------
PigSnoutSurface::PigSnoutSurface(ActionExtension * extension) : Action(extension) {
    // Setting name, description and input component
    setName("PigSnoutSurface");
    setDescription("This action allows to count white voxel from a 2D image.");
    setComponent("ImageComponent");

    // Setting classification family and tags
    setFamily("ExcellenceLab");
    // Tags allow the user to search the actions trhough themes
    // You can add tags here with the method addTag("tagName");

    // Setting the action's parameters
    // If you want to add parameters to your action, you can add them
    // using properties
    Property * propImageName = new Property("Image Name", "No Image", tr("Input image name"), "");
    propImageName->setReadOnly(true);
    Property * propNbPixels = new Property(tr("Nb Pixels"), 0, tr("Number of pixels of the pig's snout "), "");
    propNbPixels->setReadOnly(true);
    Property * propSurface = new Property(tr("Surface"), 0.0, tr("Surface of the pig's snout"), "mm^2");
    propSurface->setReadOnly(true);

    addParameter(propImageName);
    addParameter(propNbPixels);
    addParameter(propSurface);

}

// --------------- destructor -------------------
PigSnoutSurface::~PigSnoutSurface() {
    // Do not do anything yet.
    // Delete stuff if you create stuff
    // (except if you use smart pointers of course !!)
}

// --------------- apply -------------------
Action::ApplyStatus PigSnoutSurface::apply() {

    ImageComponent * input = dynamic_cast<ImageComponent *> ( dynamic_cast<ImageComponent *> (getTargets().last()));
    
    return process(input);
}

Action::ApplyStatus PigSnoutSurface::process(ImageComponent * comp) {
    // Getting the input image
    vtkSmartPointer<vtkImageData> inputImage  = comp->getImageData();
    
    // Check that the type is unsigned char and 2D image
    if (inputImage->GetScalarType() != VTK_UNSIGNED_CHAR) {
        QMessageBox::warning(NULL, "Warning", "This action is designed only for unsigned char type images\n sorry...\n");
        return ABORTED;
    }
    int * dims = inputImage->GetDimensions();
    if (dims[2] > 1) {
        QMessageBox::warning(NULL, "Warning", "This action is designed only for 2D images\n sorry...\n");
        return ABORTED;
    }

    
    // count the number of non-zero pixels:
    int nbNonZeroPixels = 0;
    for (int y = 0; y < dims[1]; y++)
    {
        for (int x = 0; x < dims[0]; x++)
        {
            unsigned char * pixel = static_cast<unsigned char *>(inputImage->GetScalarPointer(x,y,0));
            if (pixel[0] > 0) {
                nbNonZeroPixels += 1;
            }
        }
    }
    
    double surface = nbNonZeroPixels * comp->getVoxelSize().x() * comp->getVoxelSize().y();
    this->setProperty("Image Name", comp->getName());
    this->setProperty("Nb Pixels", nbNonZeroPixels);
    this->setProperty("Surface", surface);
    
    QString pigSnoutName = comp->getName() + "_stats";
    QString pigSnoutFileName = QFileInfo(comp->getFileName()).path() + "/" + pigSnoutName + ".pig";
    PigSnoutStatistics * pig = new PigSnoutStatistics(pigSnoutName, comp->getName(), nbNonZeroPixels, surface);
    pig->setFileName(pigSnoutFileName);
    

    Application::refresh();
    
    return SUCCESS;
}


