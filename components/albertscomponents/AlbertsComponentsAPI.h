// ----- FRAME_COMPONENTAPI.h
#ifndef ALBERTSCOMPONENTSAPI_H
#define ALBERTSCOMPONENTSAPI_H

/***************************************************************************
 *                                                                         *
 *                           LPR_COMPONENT_API                                    *
 *                                                                         *
 ***************************************************************************/

#if defined(_WIN32) && !defined(__MINGW32__) // MSVC only
// Visiblement, Visual ne supporte pas la sp�cification des types d'exceptions.
// Si j'ai bien compris, lorsque l'on fait un _declspec(), on a par d�faut l'attribut nothrow
// Je n'ai pas trouv� d'attribut throw. Il semblerait que Visual du coup ignore la sp�cification
// du type de l'exception. Le compilateur �crit alors un warning. La ligne suivante est pour �viter
// le warning en question. L'id�al serait que l'on puisse demander � Visual non seulemnt de prendre
// en compte l'exception, mais aussi son type, mais je n'ai trouv� null part comment faire et je ne 
// suis pas s�re que ce soit possible...
#pragma warning( disable : 4290 ) 
#endif // MSVC only

// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the COMPILE_RAW_IMAGES_API
// flag defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// CAMITK_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#if defined(_WIN32)
#ifdef COMPILE_ALBERTSCOMPONENTS_API
	#define ALBERTSCOMPONENTS_API __declspec(dllexport)
#else
	#define ALBERTSCOMPONENTS_API __declspec(dllimport)
#endif // COMPILE_ALBERTSCOMPONENTS_API

#else // for all other platforms LPRROBOT_API is defined to be "nothing"

#ifndef ALBERTSCOMPONENTS_API
	#define ALBERTSCOMPONENTS_API
#endif // ALBERTSCOMPONENTS_API

#endif // #if defined _WIN32

#endif // #ifndef ALBERTSCOMPONENTSAPI_H