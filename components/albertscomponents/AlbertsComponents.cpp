/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2014 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/


#include "AlbertsComponents.h"


// include generated components headers
#include "PigSnoutStatistics.h"

using namespace camitk;

// --------------- GetFileExtensions -------------------
QStringList AlbertsComponents::getFileExtensions() const { 
    QStringList ext;
    ext << "pig";
    
    return ext;
}

// --------------- Open -------------------
Component * AlbertsComponents::open(const QString & fileName) throw (AbortException) {
    return new PigSnoutStatistics(fileName);}

// --------------- Save --------------------
bool AlbertsComponents::save(Component * component) const { 
    PigSnoutStatistics * pig = dynamic_cast<PigSnoutStatistics *>(component);
    pig->save(pig->getFileName());

    return false;
}

