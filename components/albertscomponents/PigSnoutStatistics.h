/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2014 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/
#ifndef PIGSNOUTSTATISTICS_H
#define PIGSNOUTSTATISTICS_H

#include "AlbertsComponentsAPI.h"

#include <QObject>

#include <Component.h>
/**
 * This Component stores statistical information about a segmented snout image.
 */
class ALBERTSCOMPONENTS_API PigSnoutStatistics : public camitk::Component {

    Q_OBJECT

public:
    /// Default Constructor
    PigSnoutStatistics(const QString & file) throw(camitk::AbortException);
    
    /// Constructor giving all important information as parameter
    PigSnoutStatistics(QString name, QString imageName, int nbPixels, double surface);

    /// Default Destructor
    virtual ~PigSnoutStatistics();

    /// do nothing to init the representation, as all representation are done in the sub-component
    virtual void initRepresentation() {};
    
    virtual void save(QString filename);

};

#endif // PIGSNOUTSTATISTICS_H

