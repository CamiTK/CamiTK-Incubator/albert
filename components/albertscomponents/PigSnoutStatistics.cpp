/*****************************************************************************
 * $CAMITK_LICENCE_BEGIN$
 *
 * CamiTK - Computer Assisted Medical Intervention ToolKit
 * (c) 2001-2014 UJF-Grenoble 1, CNRS, TIMC-IMAG UMR 5525 (GMCAO)
 *
 * Visit http://camitk.imag.fr for more information
 *
 * This file is part of CamiTK.
 *
 * CamiTK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3
 * only, as published by the Free Software Foundation.
 *
 * CamiTK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License version 3 for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * version 3 along with CamiTK.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $CAMITK_LICENCE_END$
 ****************************************************************************/

// CamiTK includes
#include "PigSnoutStatistics.h"
#include <Property.h>

#include <QFileInfo>

#include <QDomDocument>
#include <QDomNode>
#include <QDomText>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>


using namespace camitk;

// --------------- Constructor -------------------
PigSnoutStatistics::PigSnoutStatistics(QString name, QString imageName, int nbPixels, double surface)
: Component("NoFile", name)
{
    addProperty(new Property("Image Name", imageName, tr("Input image name"), ""));
    addProperty(new Property(tr("Nb Pixels"), nbPixels, tr("Number of pixels of the pig's snout "), ""));
    addProperty(new Property(tr("Surface"), surface, tr("Surface of the pig's snout"), "mm^2"));
    
}

PigSnoutStatistics::PigSnoutStatistics(const QString & filename) throw(AbortException)
:
Component(filename, QFileInfo(filename).baseName())
{
    // Read the input file...
	QString msg;
	QDomDocument doc;
    
	//Check the file validity
	QFile file(filename);
	if (!file.open(QIODevice::ReadOnly)) {
		msg = "File not found: the file " + filename + " was not found";
		CAMITK_ERROR("PigSnoutStatistics", "PigSnoutStatistics", msg.toStdString());
		msg = "Error: cannot find file " + filename + " sorry...\n";
		throw camitk::AbortException (msg.toStdString());
	}
	if (!doc.setContent(&file)) {
		msg = "File " + filename + " does not have a valid root (not a valid XML file format).\n";
		file.close();
		CAMITK_ERROR("PigSnoutStatistics", "PigSnoutStatistics", msg.toStdString());
		throw camitk::AbortException(msg.toStdString());
	}
	
	QString rootName = doc.documentElement().nodeName();
	if (rootName != QString("pigSnoutStat")) {
		file.close();
		msg = "File " + filename + " is not in the valid pigSnoutStat file format.\n";
		CAMITK_ERROR("PigSnoutStatistics", "PigSnoutStatistics", msg.toStdString());
		throw camitk::AbortException(msg.toStdString());
	}
    
	file.close();

    
    // Set the properties with their default values
    QDomElement root = doc.documentElement();
    QString imageName = root.elementsByTagName("imageName").item(0).toElement().text();
    addProperty(new Property("Image Name", imageName, tr("Input image name"), ""));
    
    int nbPixels = root.elementsByTagName("nbPixels").item(0).toElement().text().toInt();
    addProperty(new Property(tr("Nb Pixels"), nbPixels, tr("Number of pixels of the pig's snout "), ""));
    double surface = root.elementsByTagName("surface").item(0).toElement().text().toDouble();
    addProperty(new Property(tr("Surface"), surface, tr("Surface of the pig's snout"), "mm^2"));

}

// --------------- destructor -------------------
PigSnoutStatistics::~PigSnoutStatistics() {

}

void PigSnoutStatistics::save(QString filename) {
    QDomDocument doc;
    QDomElement root = doc.createElement("pigSnoutStat");
    doc.appendChild(root);
    
    QDomElement imageNameElement = doc.createElement("imageName");
    QDomText imageNameText = doc.createTextNode(property("Image Name").toString());
    imageNameElement.appendChild(imageNameText);
    root.appendChild(imageNameElement);
    
    QDomElement nbPixelsElement = doc.createElement("nbPixels");
    QDomText nbPixelsText = doc.createTextNode(property("Nb Pixels").toString());
    nbPixelsElement.appendChild(nbPixelsText);
    root.appendChild(nbPixelsElement);
    
    QDomElement surfaceElement = doc.createElement("surface");
    QDomText surfaceText = doc.createTextNode(property("Surface").toString());
    surfaceElement.appendChild(surfaceText);
    root.appendChild(surfaceElement);
    
    QFile outFile( filename );
    if( !outFile.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        CAMITK_DEBUG("PigSnoutStatistics", "save", "Failed to open file " + filename + " for writing." );
        return;
    }
    
    QTextStream stream( &outFile );
    stream.setCodec(QTextCodec::codecForName("UTF-8"));
    doc.save(stream, 4, QDomNode::EncodingFromTextStream);
    
    outFile.close();

    
}

